package com.example.cicdgitlab.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CICDEntrypoint {

    @GetMapping
    public ResponseEntity<String> hello(){
        return new ResponseEntity<>("hello world hope everybody coding there !!!", HttpStatus.OK);
    }
}
